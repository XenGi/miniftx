![MiniFTX][mftx]
![MiniFTX3D][mftx3d]

# MiniFTX

The MiniFTX is a low-cost, reconfigurable, and compact Micro-USB breakout board based on the FTDI FT230X full-speed USB Serial UART IC.

Here are some pictures of the finished board: [imgur][imgur]

---

This project is a redesign of the [MicroFTX from Jim Paris](http://jim.sh/ftx/). I rebuild the board in KiCAD and added RX and TX LEDs.


[mftx]: microftx.png?raw=true
[mftx3d]: microftx_3d.png?raw=true
[imgur]: http://imgur.com/a/OmxJY
