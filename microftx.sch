EESchema Schematic File Version 2
LIBS:microftx-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ft230x
LIBS:microftx-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L USB_OTG USB1
U 1 1 56FDC38D
P 3000 3950
F 0 "USB1" H 3325 3825 50  0000 C CNN
F 1 "USB_OTG" H 3000 4150 50  0000 C CNN
F 2 "microftx_footprints:10104110-0001LF" V 2950 3850 50  0001 C CNN
F 3 "10104110-0001LF" V 2950 3850 50  0001 C CNN
	1    3000 3950
	0    -1   1    0   
$EndComp
$Comp
L GND #PWR01
U 1 1 56FDC418
P 3450 4450
F 0 "#PWR01" H 3450 4200 50  0001 C CNN
F 1 "GND" H 3450 4300 50  0000 C CNN
F 2 "" H 3450 4450 50  0000 C CNN
F 3 "" H 3450 4450 50  0000 C CNN
	1    3450 4450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 56FDCA2F
P 4850 4500
F 0 "#PWR02" H 4850 4250 50  0001 C CNN
F 1 "GND" H 4850 4350 50  0000 C CNN
F 2 "" H 4850 4500 50  0000 C CNN
F 3 "" H 4850 4500 50  0000 C CNN
	1    4850 4500
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 56FDCAF4
P 3600 3850
F 0 "R2" V 3680 3850 50  0000 C CNN
F 1 "R27" V 3600 3850 50  0000 C CNN
F 2 "microftx_footprints:CPF-A-0603B27RE1" V 3530 3850 50  0001 C CNN
F 3 "CPF-A-0603B27RE1" H 3600 3850 50  0001 C CNN
	1    3600 3850
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 56FDCB21
P 4000 3950
F 0 "R1" V 4080 3950 50  0000 C CNN
F 1 "R27" V 4000 3950 50  0000 C CNN
F 2 "microftx_footprints:CPF-A-0603B27RE1" V 3930 3950 50  0001 C CNN
F 3 "CPF-A-0603B27RE1" H 4000 3950 50  0001 C CNN
	1    4000 3950
	0    1    1    0   
$EndComp
$Comp
L C C3
U 1 1 56FDCBD6
P 3800 4450
F 0 "C3" H 3825 4550 50  0000 L CNN
F 1 "C.1" H 3825 4350 50  0000 L CNN
F 2 "microftx_footprints:0603B104J160CT" H 3838 4300 50  0001 C CNN
F 3 "0603B104J160CT" H 3800 4450 50  0001 C CNN
	1    3800 4450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 56FDCC17
P 3800 4700
F 0 "#PWR03" H 3800 4450 50  0001 C CNN
F 1 "GND" H 3800 4550 50  0000 C CNN
F 2 "" H 3800 4700 50  0000 C CNN
F 3 "" H 3800 4700 50  0000 C CNN
	1    3800 4700
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 56FDCC35
P 4250 4450
F 0 "C1" H 4275 4550 50  0000 L CNN
F 1 "C.1" H 4275 4350 50  0000 L CNN
F 2 "microftx_footprints:0603B104J160CT" H 4288 4300 50  0001 C CNN
F 3 "0603B104J160CT" H 4250 4450 50  0001 C CNN
	1    4250 4450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 56FDCC68
P 4250 4700
F 0 "#PWR04" H 4250 4450 50  0001 C CNN
F 1 "GND" H 4250 4550 50  0000 C CNN
F 2 "" H 4250 4700 50  0000 C CNN
F 3 "" H 4250 4700 50  0000 C CNN
	1    4250 4700
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 P1
U 1 1 56FDD077
P 7000 3600
F 0 "P1" H 7000 3850 50  0000 C CNN
F 1 "CONN_01X04" V 7100 3600 50  0000 C CNN
F 2 "microftx_footprints:Pin_Header_Straight_1x04" H 7000 3600 50  0001 C CNN
F 3 "" H 7000 3600 50  0000 C CNN
	1    7000 3600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 56FDD0B0
P 6750 3800
F 0 "#PWR05" H 6750 3550 50  0001 C CNN
F 1 "GND" H 6750 3650 50  0000 C CNN
F 2 "" H 6750 3800 50  0000 C CNN
F 3 "" H 6750 3800 50  0000 C CNN
	1    6750 3800
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 56FDD217
P 5600 2950
F 0 "C2" H 5625 3050 50  0000 L CNN
F 1 "C2.2" H 5625 2850 50  0000 L CNN
F 2 "microftx_footprints:0603X225K100CT" H 5638 2800 50  0001 C CNN
F 3 "0603X225K100CT" H 5600 2950 50  0001 C CNN
	1    5600 2950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 56FDD24A
P 5600 2750
F 0 "#PWR06" H 5600 2500 50  0001 C CNN
F 1 "GND" H 5600 2600 50  0000 C CNN
F 2 "" H 5600 2750 50  0000 C CNN
F 3 "" H 5600 2750 50  0000 C CNN
	1    5600 2750
	-1   0    0    1   
$EndComp
$Comp
L Jumper_NO_Small JP1
U 1 1 56FDD444
P 3550 3750
F 0 "JP1" H 3550 3830 50  0000 C CNN
F 1 "Jumper_NO_Small" H 3560 3690 50  0001 C CNN
F 2 "microftx_footprints:Solder_Jumper" H 3550 3750 50  0001 C CNN
F 3 "" H 3550 3750 50  0000 C CNN
	1    3550 3750
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NO_Small JP2
U 1 1 56FDDF71
P 4350 3150
F 0 "JP2" H 4350 3230 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4360 3090 50  0001 C CNN
F 2 "microftx_footprints:Solder_Jumper" H 4350 3150 50  0001 C CNN
F 3 "" H 4350 3150 50  0000 C CNN
	1    4350 3150
	-1   0    0    1   
$EndComp
$Comp
L Jumper_NO_Small JP4
U 1 1 56FDE2AD
P 4700 3550
F 0 "JP4" H 4700 3630 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4710 3490 50  0001 C CNN
F 2 "microftx_footprints:Solder_Jumper" H 4700 3550 50  0001 C CNN
F 3 "" H 4700 3550 50  0000 C CNN
	1    4700 3550
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NO_Small JP3
U 1 1 56FDE4F2
P 4850 3300
F 0 "JP3" H 4850 3380 50  0000 C CNN
F 1 "Jumper_NO_Small" H 4860 3240 50  0001 C CNN
F 2 "microftx_footprints:Solder_Jumper" H 4850 3300 50  0001 C CNN
F 3 "" H 4850 3300 50  0000 C CNN
	1    4850 3300
	0    1    1    0   
$EndComp
NoConn ~ 3300 4050
$Comp
L CONN_01X03 P2
U 1 1 56FDF792
P 7000 4200
F 0 "P2" H 7000 4400 50  0000 C CNN
F 1 "CONN_01X03" V 7100 4200 50  0000 C CNN
F 2 "microftx_footprints:Pin_Header_Straight_1x03" V 7000 4200 50  0001 C CNN
F 3 "" H 7000 4200 50  0000 C CNN
	1    7000 4200
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P3
U 1 1 56FDF7DF
P 7000 4700
F 0 "P3" H 7000 4900 50  0000 C CNN
F 1 "CONN_01X03" V 7100 4700 50  0000 C CNN
F 2 "microftx_footprints:Pin_Header_Straight_1x03" H 7000 4700 50  0001 C CNN
F 3 "" H 7000 4700 50  0000 C CNN
	1    7000 4700
	1    0    0    -1  
$EndComp
Text GLabel 6000 4050 2    60   Input ~ 0
CBUS0
Text GLabel 6000 4150 2    60   Input ~ 0
CBUS1
Text GLabel 6000 4350 2    60   Input ~ 0
CBUS3
Text GLabel 6800 4200 0    60   Input ~ 0
CBUS0
Text GLabel 6800 4300 0    60   Input ~ 0
CBUS1
Text GLabel 6800 4100 0    60   Input ~ 0
CBUS3
Text GLabel 6800 4600 0    60   Input ~ 0
RTS
Text GLabel 6800 4700 0    60   Input ~ 0
CTS
Text GLabel 6800 4800 0    60   Input ~ 0
CBUS2
Text GLabel 6000 3850 2    60   Input ~ 0
RTS
Text GLabel 6000 3950 2    60   Input ~ 0
CTS
Text GLabel 6000 4250 2    60   Input ~ 0
CBUS2
$Comp
L Led_Small D2
U 1 1 56FE113B
P 5550 2300
F 0 "D2" H 5500 2425 50  0000 L CNN
F 1 "RX_Green" H 5375 2200 50  0000 L CNN
F 2 "microftx_footprints:KPC-3216SGC" V 5550 2300 50  0001 C CNN
F 3 "KPC-3216SGC" V 5550 2300 50  0001 C CNN
	1    5550 2300
	-1   0    0    1   
$EndComp
$Comp
L Led_Small D1
U 1 1 56FE119C
P 7000 2050
F 0 "D1" H 6950 2175 50  0000 L CNN
F 1 "TX_Red" H 6825 1950 50  0000 L CNN
F 2 "microftx_footprints:KPC-3216SGC" V 7000 2050 50  0001 C CNN
F 3 "MCL-S250SRC" V 7000 2050 50  0001 C CNN
	1    7000 2050
	0    -1   -1   0   
$EndComp
$Comp
L R R3
U 1 1 56FEDF96
P 5850 2300
F 0 "R3" V 5930 2300 50  0000 C CNN
F 1 "R68" V 5850 2300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5780 2300 50  0001 C CNN
F 3 "MC0063W0603168R" H 5850 2300 50  0001 C CNN
	1    5850 2300
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 56FEE022
P 7000 2350
F 0 "R4" V 7080 2350 50  0000 C CNN
F 1 "R150" V 7000 2350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6930 2350 50  0001 C CNN
F 3 "MC0063W06031150R" H 7000 2350 50  0001 C CNN
	1    7000 2350
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR07
U 1 1 56FEEB02
P 5400 2300
F 0 "#PWR07" H 5400 2150 50  0001 C CNN
F 1 "VCC" H 5400 2450 50  0000 C CNN
F 2 "" H 5400 2300 50  0000 C CNN
F 3 "" H 5400 2300 50  0000 C CNN
	1    5400 2300
	0    -1   -1   0   
$EndComp
$Comp
L VCC #PWR08
U 1 1 56FEEB4C
P 7000 1900
F 0 "#PWR08" H 7000 1750 50  0001 C CNN
F 1 "VCC" H 7000 2050 50  0000 C CNN
F 2 "" H 7000 1900 50  0000 C CNN
F 3 "" H 7000 1900 50  0000 C CNN
	1    7000 1900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR09
U 1 1 56FF0F34
P 3350 3600
F 0 "#PWR09" H 3350 3450 50  0001 C CNN
F 1 "VCC" H 3350 3750 50  0000 C CNN
F 2 "" H 3350 3600 50  0000 C CNN
F 3 "" H 3350 3600 50  0000 C CNN
	1    3350 3600
	1    0    0    -1  
$EndComp
Connection ~ 4550 3550
Wire Wire Line
	4850 3650 4900 3650
Connection ~ 3350 3750
Wire Wire Line
	3350 3600 3350 3750
Connection ~ 3700 3750
Wire Wire Line
	7000 1900 7000 1950
Wire Wire Line
	5400 2300 5450 2300
Connection ~ 6600 3650
Connection ~ 6250 3550
Wire Wire Line
	2900 4400 3450 4400
Wire Wire Line
	2900 4350 2900 4400
Wire Wire Line
	4550 3550 4550 3850
Wire Wire Line
	4250 3550 4600 3550
Connection ~ 4850 3550
Wire Wire Line
	4800 3550 4850 3550
Wire Wire Line
	6750 3150 4450 3150
Connection ~ 4850 3150
Wire Wire Line
	4850 3200 4850 3150
Connection ~ 5600 3150
Wire Wire Line
	5600 3100 5600 3150
Wire Wire Line
	5600 2750 5600 2800
Wire Wire Line
	6750 3450 6750 3150
Wire Wire Line
	6800 3450 6750 3450
Wire Wire Line
	4150 3150 4250 3150
Connection ~ 4150 3750
Wire Wire Line
	4150 3150 4150 3750
Wire Wire Line
	3300 3750 3450 3750
Wire Wire Line
	4850 3400 4850 3650
Wire Wire Line
	4550 3850 4900 3850
Wire Wire Line
	3650 3750 4900 3750
Wire Wire Line
	4300 3950 4900 3950
Wire Wire Line
	4300 3850 4300 3950
Wire Wire Line
	3750 3850 4300 3850
Wire Wire Line
	4200 4050 4900 4050
Wire Wire Line
	4200 3950 4200 4050
Wire Wire Line
	4150 3950 4200 3950
Wire Wire Line
	3300 3950 3850 3950
Wire Wire Line
	3300 3850 3450 3850
Connection ~ 3800 3750
Wire Wire Line
	3800 4300 3800 3750
Wire Wire Line
	4250 3550 4250 4300
Wire Wire Line
	6050 3750 6000 3750
Wire Wire Line
	6050 3550 6050 3750
Wire Wire Line
	6800 3550 6050 3550
Wire Wire Line
	6000 3650 6800 3650
Wire Wire Line
	6750 3750 6750 3800
Wire Wire Line
	6800 3750 6750 3750
Wire Wire Line
	4250 4600 4250 4700
Wire Wire Line
	3800 4600 3800 4700
Connection ~ 4850 4350
Wire Wire Line
	4850 4250 4900 4250
Connection ~ 4850 4450
Wire Wire Line
	4850 4350 4900 4350
Wire Wire Line
	4850 4450 4900 4450
Wire Wire Line
	4850 4250 4850 4500
Connection ~ 3450 4400
Wire Wire Line
	3300 4150 3450 4150
Wire Wire Line
	3450 4150 3450 4450
Wire Wire Line
	4900 4150 4850 4150
Wire Wire Line
	4850 4150 4850 3750
Connection ~ 4850 3750
$Comp
L FT230XQ U1
U 1 1 56FEEE5D
P 5450 4050
F 0 "U1" H 5450 4550 60  0000 C CNN
F 1 "FT230XQ" H 5450 3550 60  0000 C CNN
F 2 "microftx_footprints:FT230XQ" H 5500 4000 60  0001 C CNN
F 3 "FT230XQ" H 5500 4000 60  0001 C CNN
	1    5450 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2300 5700 2300
Wire Wire Line
	7000 2150 7000 2200
Wire Wire Line
	6000 2300 6050 2300
Wire Wire Line
	7000 2500 7000 2550
$Comp
L GND #PWR010
U 1 1 57019D16
P 6500 2300
F 0 "#PWR010" H 6500 2050 50  0001 C CNN
F 1 "GND" H 6500 2150 50  0000 C CNN
F 2 "" H 6500 2300 50  0000 C CNN
F 3 "" H 6500 2300 50  0000 C CNN
	1    6500 2300
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR011
U 1 1 57019D52
P 7000 3000
F 0 "#PWR011" H 7000 2750 50  0001 C CNN
F 1 "GND" H 7000 2850 50  0000 C CNN
F 2 "" H 7000 3000 50  0000 C CNN
F 3 "" H 7000 3000 50  0000 C CNN
	1    7000 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2300 6500 2300
Wire Wire Line
	7000 2950 7000 3000
Wire Wire Line
	6250 2600 6250 3550
Wire Wire Line
	6700 2750 6600 2750
Wire Wire Line
	6600 2750 6600 3650
$Comp
L Q_NMOS_GSD Q1
U 1 1 5702F3B3
P 6900 2750
F 0 "Q1" H 7200 2800 50  0000 R CNN
F 1 "Q_NMOS_GSD" H 7550 2700 50  0000 R CNN
F 2 "microftx_footprints:NTS4001NT1G" H 7100 2850 50  0001 C CNN
F 3 "NTS4001NT1G" H 6900 2750 50  0001 C CNN
	1    6900 2750
	1    0    0    -1  
$EndComp
$Comp
L Q_NMOS_GSD Q2
U 1 1 5702F623
P 6250 2400
F 0 "Q2" H 6550 2450 50  0000 R CNN
F 1 "Q_NMOS_GSD" H 6900 2350 50  0000 R CNN
F 2 "microftx_footprints:NTS4001NT1G" H 6450 2500 50  0001 C CNN
F 3 "NTS4001NT1G" H 6250 2400 50  0001 C CNN
	1    6250 2400
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
